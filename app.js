const express = require('express')
const app = express()
const port = 8080

const users = [
    {
        id: '1',
        name: 'Bob'
    },
    {
        id: '2',
        name: 'Alice'
    },
    {
        id: '3',
        name: 'Charlie'
    },
    {
        id: '4',
        name: 'Poe'
    },
    {
        id: '5',
        name: 'Steve'
    }
]

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/users', (req, res) => {
    res.send(users)
  })

app.listen(port, () => {
  console.log(`API app listening at http://localhost:${port}`)
})